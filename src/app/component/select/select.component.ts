import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css']
})
export class SelectComponent implements OnInit {

  @Input() show: boolean = false;

  constructor() { }

  ngOnInit() {
  }
  
  @Output() onSelect = new EventEmitter<string>();

  select(data) {
    this.show = false;
    console.log(data);
    this.onSelect.emit(data);
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RightNotificationComponent } from './right-notification.component';

describe('RightNotificationComponent', () => {
  let component: RightNotificationComponent;
  let fixture: ComponentFixture<RightNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RightNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RightNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RightAdvancedSearchComponent } from './right-advanced-search.component';

describe('RightAdvancedSearchComponent', () => {
  let component: RightAdvancedSearchComponent;
  let fixture: ComponentFixture<RightAdvancedSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RightAdvancedSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RightAdvancedSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { AgmCoreModule } from '@agm/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BrowserModule } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { ngfModule, ngf } from "angular-file";
import { NgSelectModule } from '@ng-select/ng-select';
//import { Ng5FilesModule } from './ng5-files';

import { BigBtnComponent } from './big-btn/big-btn.component';
import { GoogleMapAreaComponent } from './google-map-area/google-map-area.component';
import { SearchHeaderComponent } from './search-header/search-header.component';
import { LeftFullMenuComponent } from './left-full-menu/left-full-menu.component';
import { LeftMenuComponent } from './left-menu/left-menu.component';
import { RightUploadAreaComponent } from './right-upload-area/right-upload-area.component';
import { GrandTableComponent } from './grand-table/grand-table.component';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { BtnCheckboxComponent } from './btn-checkbox/btn-checkbox.component';
import { SelectComponent } from './select/select.component';
import { RightAdvancedSearchComponent } from './right-advanced-search/right-advanced-search.component';
import { RightSettingsComponent } from './right-settings/right-settings.component';
import { RightNotificationComponent } from './right-notification/right-notification.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDPC_9kS4afWzqzwjbOJ_PEo8PJ8KBIPDs'
    }),
    PerfectScrollbarModule,
    BrowserModule,
    ngfModule,
    NgSelectModule/*,
    Ng5FilesModule*/
  ],
  exports: [
    BigBtnComponent,
    GoogleMapAreaComponent,
    SearchHeaderComponent,
    LeftFullMenuComponent,
    LeftMenuComponent,
    RightUploadAreaComponent,
    BtnCheckboxComponent,
    GrandTableComponent,
    RightAdvancedSearchComponent,
    RightSettingsComponent,
    RightNotificationComponent
  ],
  declarations: [
    BigBtnComponent,
    GoogleMapAreaComponent,
    SearchHeaderComponent,
    LeftFullMenuComponent,
    LeftMenuComponent,
    RightUploadAreaComponent,
    BtnCheckboxComponent,
    SelectComponent,
    GrandTableComponent,
    RightAdvancedSearchComponent,
    RightSettingsComponent,
    RightNotificationComponent
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ]
})

export class ComponentModule { }


platformBrowserDynamic().bootstrapModule(ComponentModule);

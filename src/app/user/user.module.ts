import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SimpleNotificationsModule } from 'angular2-notifications';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import { ComponentModule } from '../component/component.module';

import { UserComponent } from './user.component';
import { MainComponent } from './main/main.component';


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};


export const routes: Routes = [
  {
    component: UserComponent,
    path: '',
    children: [
      {
        path: '',
        component: MainComponent
      }
    ]
  }
]

@NgModule({
  imports: [
    ComponentModule,
    CommonModule,
    RouterModule.forRoot(routes),
    SimpleNotificationsModule.forRoot(),
    PerfectScrollbarModule
  ],
  declarations: [
    UserComponent, 
    MainComponent
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ]
})
export class UserModule { }

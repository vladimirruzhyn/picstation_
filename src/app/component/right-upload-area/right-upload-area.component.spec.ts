import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RightUploadAreaComponent } from './right-upload-area.component';

describe('RightUploadAreaComponent', () => {
  let component: RightUploadAreaComponent;
  let fixture: ComponentFixture<RightUploadAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RightUploadAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RightUploadAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

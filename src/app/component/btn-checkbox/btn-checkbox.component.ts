import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-btn-checkbox',
  templateUrl: './btn-checkbox.component.html',
  styleUrls: ['./btn-checkbox.component.css']
})
export class BtnCheckboxComponent implements OnInit {

  selectRate: boolean = false;
  selectAuction: boolean = false;

  @Output() onClick = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  select(data) {
    switch(data) {
      case 'rate':
        this.selectAuction = false;
        this.selectRate = true;
        break;
      case 'auction':
        this.selectAuction = true;
        this.selectRate = false;
        break;
    }
    this.onClick.emit(data);
    //return data;
  }

}

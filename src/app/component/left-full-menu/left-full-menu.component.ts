import { Component, OnInit, Input, OnChanges } from '@angular/core';
declare var require: any;

@Component({
  selector: 'app-left-full-menu',
  templateUrl: './left-full-menu.component.html',
  styleUrls: ['./left-full-menu.component.css']
})
export class LeftFullMenuComponent implements OnInit {

  bitmapPath = require('./assets/bitmap.png');
  dashboardPath = require('./assets/dashboard.svg');
  dialoguePath = require('./assets/dialogue.svg');
  filesPath = require('./assets/files.svg');
  favoritesPath = require('./assets/favorites.svg');
  orderPath = require('./assets/order.svg');
  statsPath = require('./assets/stats.svg');
  shapeCopyPath = require('./assets/combined-shape-copy.png');

  constructor() { }

  ngOnInit() {
  }

}

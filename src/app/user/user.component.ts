import { Component, OnInit, OnChanges } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  public fullMenuStatus = false;
  upload: boolean = false;
  advancedSearch: boolean = false;
  notification: boolean = false;
  settings: boolean = false;
  
  constructor() { }

  ngOnInit() {
  }

  changeMenu() {
    this.fullMenuStatus = !this.fullMenuStatus;
  }
  clickingOnContent($event) {
    this.fullMenuStatus = false;
  }
  changeRightMenu(link: string) {
    switch (link) {
      case "upload":
        this.upload = !this.upload;
        this.advancedSearch = false;
        this.notification = false;
        this.settings = false;
        break;
      case "advancedSearch":
        this.upload = false;
        this.advancedSearch = !this.advancedSearch;
        this.notification = false;
        this.settings = false;
        break;
      case "notification":
        this.upload = false;
        this.advancedSearch = false;
        this.notification = !this.notification;
        this.settings = false;
        break;
      case "settings":
        this.upload = false;
        this.advancedSearch = false;
        this.notification = false;
        this.settings = !this.settings;
        break;
      default:
        this.upload = false;
        this.advancedSearch = false;
        this.notification = false;
        this.settings = false;
        break;
    }
  }

}

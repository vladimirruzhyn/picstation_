import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-google-map-area',
  templateUrl: './google-map-area.component.html',
  styleUrls: ['./google-map-area.component.css']
})
export class GoogleMapAreaComponent implements OnInit {

  @Input() lat: number = 51.678418;
  @Input() lng: number = 7.809007;
  @Input() width: string = '200px';
  @Input() height: string = '200px';
  markerLat: number;
  markerLng: number;

  constructor() { }

  ngOnInit() {
    this.markerLat = this.lat;
    this.markerLng = this.lng;
  }

  @Output() coordsChange = new EventEmitter<Object>();
  
  mapClicked(data) {
    this.markerLat = data['coords']['lat']
    this.markerLng = data['coords']['lng']
    this.coordsChange.emit({lat: this.markerLat, lng: this.markerLng});
    console.log(this.markerLat);
    console.log(this.markerLng);
  }

  markerDragEnd(data) {
    this.markerLat = data['coords']['lat']
    this.markerLng = data['coords']['lng']
    this.coordsChange.emit({lat: this.markerLat, lng: this.markerLng});
  }

}

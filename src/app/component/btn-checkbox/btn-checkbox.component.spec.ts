import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnCheckboxComponent } from './btn-checkbox.component';

describe('BtnCheckboxComponent', () => {
  let component: BtnCheckboxComponent;
  let fixture: ComponentFixture<BtnCheckboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnCheckboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

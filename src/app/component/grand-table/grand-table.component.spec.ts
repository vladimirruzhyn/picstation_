import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrandTableComponent } from './grand-table.component';

describe('GrandTableComponent', () => {
  let component: GrandTableComponent;
  let fixture: ComponentFixture<GrandTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrandTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrandTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

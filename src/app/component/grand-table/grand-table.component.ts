import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-grand-table',
  templateUrl: './grand-table.component.html',
  styleUrls: ['./grand-table.component.css']
})
export class GrandTableComponent implements OnInit {
  keys = [];
  @Input() width: string;

  header = [
    {
        title: 'id',
        key: 'id',
        width: '100px'
    },
    {
        title: 'title',
        key: 'title',
        width: '200px'
    },
    {
        title: 'content',
        key: 'content',
        width: '300px'
    }
  ]
  body = [
    {
        id: 11,
        title:'dfgdfgfdg',
        content: 'sdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfgsdfg'
    },
    {
        id: 12,
        title:'dfgdfgfdg',
        content: 'sdfgsdfg'
    },
    {
        id: 13,
        title:'dfgdfgfdg',
        content: 'sdfgsdfg'
    },
    {
        id: 14,
        title:'dfgdfgfdg',
        content: 'sdfgsdfg'
    },
    {
        id: 15,
        title:'dfgdfgfdg',
        content: 'sdfgsdfg'
    }
  ]
  actionArray = [
    {
        
        bgColor:'#00917e',
        hoverBgColor:'white',
        text: 'button',
        action:'bnmmj',
        key:'id'
    },
    {
        color:'wheat',
        hoverColor:'blue',
        bgColor:'#00917e',
        hoverBgColor:'white',
        text: 'button',
        action:'bnmmj',
        key:'id'
    },
    {
        color:'wheat',
        hoverColor:'blue',
        bgColor:'#00917e',
        hoverBgColor:'white',
        text: 'button',
        action:'bnmmj',
        key:'id'
    },
    {
        color:'wheat',
        hoverColor:'blue',
        bgColor:'#00917e',
        hoverBgColor:'white',
        text: 'button',
        action:'bnmmj',
        key:'id'
    },
    {
        color:'wheat',
        hoverColor:'blue',
        bgColor:'#00917e',
        hoverBgColor:'white',
        text: 'button',
        action:'bnmmj',
        key:'id'
    }
  ]
  constructor() { }

  ngOnInit() {
    for (let i = 0; i < this.header.length; i++) {
      const element = this.header[i];
      this.keys[i] = {'key':element.key, width: element.width};
    }
      
  }

  click(action, value) {
    console.log(action);
    console.log(value)
  }

}

import { Component, OnInit } from '@angular/core';
declare var require: any;

@Component({
  selector: 'app-left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.css']
})
export class LeftMenuComponent implements OnInit {

  bitmapPath = require('./assets/bitmap.png');
  dashboardPath = require('./assets/dashboard.svg');
  dialoguePath = require('./assets/dialogue.svg');
  filesPath = require('./assets/files.svg');
  favoritesPath = require('./assets/favorites.svg');
  orderPath = require('./assets/order.svg');
  statsPath = require('./assets/stats.svg');

  constructor() { }

  ngOnInit() {
  }

}

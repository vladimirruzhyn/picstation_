import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftFullMenuComponent } from './left-full-menu.component';

describe('LeftFullMenuComponent', () => {
  let component: LeftFullMenuComponent;
  let fixture: ComponentFixture<LeftFullMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftFullMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftFullMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

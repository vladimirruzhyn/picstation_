import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoogleMapAreaComponent } from './google-map-area.component';

describe('GoogleMapAreaComponent', () => {
  let component: GoogleMapAreaComponent;
  let fixture: ComponentFixture<GoogleMapAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoogleMapAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoogleMapAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

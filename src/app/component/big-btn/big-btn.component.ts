import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-big-btn',
  templateUrl: './big-btn.component.html',
  styleUrls: ['./big-btn.component.css']
})
export class BigBtnComponent implements OnInit {
  @Input() bgColor: string;
  @Input() bgHovColor: string;
  @Input() textColor: string;
  @Input() textHovColor: string;
  @Input() margin: number;
  hover: boolean = false;
  bgColorF: string;
  textColorF: string;
  constructor() { }
  hovDiv(){
    this.hover = !this.hover;
    if (this.hover) {
      this.bgColorF = this.bgHovColor;
      this.textColorF = this.textHovColor;
      } else { 
      this.bgColorF = this.bgColor;
      this.textColorF = this.textColor;
    }
  }
  @Output() onChanged = new EventEmitter<boolean>();
    change() {
        this.onChanged.emit();
    }
  ngOnInit() {
    this.bgColorF = this.bgColor;
    this.textColorF = this.textColor;
  }
}

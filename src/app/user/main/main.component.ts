import { Component, OnInit } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  inputLat= 50.535103306175095
  inputLng= 30.260078371912186;
  lat = '';
  lng = '';

  constructor(
    private _notifications: NotificationsService
  ) { }

  ngOnInit() {
  }

  showNot() {
    console.log('test');
    let tmp = {
      animate: "fromRight",
      clickToClose: true,
      pauseOnHover: true,
      showProgressBar: true,
      timeOut: 5000
    }
    console.log(this._notifications);
    this._notifications.create('test', 'test', 'success', tmp)
  }

  coordsChange(data) {
    this.lat = data['lat'];
    this.lng = data['lng'];
  }

}

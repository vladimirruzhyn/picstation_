import { Component, OnInit } from '@angular/core';
declare var require: any;


@Component({
  selector: 'app-right-upload-area',
  templateUrl: './right-upload-area.component.html',
  styleUrls: ['./right-upload-area.component.css']
})
export class RightUploadAreaComponent implements OnInit {

  uploadImagePath = require('./assets/upload-map.png');
  ddImagePath = require('./assets/drag-and-drop.svg');
  chewronePath = require('./assets/combined-shape-copy.png');

  files: File[] = [];
  file: File;
  validComboDrag;
  invalidComboDrag;
  showSelect = false;
  simpleItems = ['item1', 'item2', 'item3'];
  selectedSimpleItem;
  searchable = false;
  inputPrice: string;
  text: string;
  auction: boolean;
  lat: number = 51.52113530081688;
  lng: number = 30.254585207849686;

  constructor() { }

  ngOnInit() {
  }

  lastFileAt($event) {
    console.log($event);
    console.log(this.files);
    console.log(this.file);
  }
  changeAuction(e: string) {
    if (e == "rate") {
      this.auction = false;
    } else {
      this.auction = true;
    }
  }
  errors = [];
  noErrors = [];
  checking() {
    if (this.text == "" || this.text == undefined) {
      this.errors.push("text-input");
    } else {
      this.noErrors.push("text-input");
    }
    if (this.selectedSimpleItem == "" || this.selectedSimpleItem == undefined || this.selectedSimpleItem == null) {
      this.errors.push("test")
    } else {
      this.noErrors.push("test");
    }
    if (Number(this.inputPrice)) {
      this.noErrors.push("input-price");
    } else {
      this.errors.push("input-price");
    }
    if (this.auction == undefined) {
      this.errors.push("sale")
      this.errors.push("sale auction")
    } else {
      this.noErrors.push("sale auction");
      this.noErrors.push("sale");
    }
    if (this.files.length == 0) {
      this.errors.push("dowload-box")
    } else {
      this.noErrors.push("dowload-box");
    }
    if (this.errors.length != 0) {
      for (let i = 0; i < this.errors.length; i++) {
        this.outline(document.getElementsByClassName(this.errors[i])[0], "red");
      }
      for (let i = 0; i < this.noErrors.length; i++) {
        this.outline(document.getElementsByClassName(this.noErrors[i])[0], "black");
      }
    } else {
      this.upload();
      for (let i = 0; i < this.noErrors.length; i++) {
        this.outline(document.getElementsByClassName(this.noErrors[i])[0], "green");
      }
    }
    this.errors = [];
    this.noErrors = []; 
  }
  coordsChange(data) {
    this.lat = data['lat'];
    this.lng = data['lng'];
  }
  outline(element, color: string) {
    element.style.border = "1px " + color + " solid";
  }
  upload() {
    console.log({
      textArea: this.text,
      periodAuction: this.selectedSimpleItem,
      price: this.inputPrice,
      auction: this.auction,
      files: this.files,
      mapCoords: {
        lat: this.lat,
        lng: this.lng
      }
    });
  }

  fileOver($event) {
    console.log($event);
  }

  clickMain($event) {
    /*console.log($event);
    console.log(event.target as Element);
    console.log(typeof $event.target);*/
    let element = $event.target;
    /*console.log(element.getElementsByClassName('header-select-find'));
    console.log(document.getElementById('header-select'));*/
    //console.log(element.getAttribute('class'));
    //console.log(element.getAttribute('class') == 'header-select-find');

  }

}

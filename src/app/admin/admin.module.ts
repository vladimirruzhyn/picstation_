import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { ComponentModule } from '../component/component.module';

import { MainComponent } from './main/main.component';
import { AdminComponent } from './admin.component';
import { TagListComponent } from './tag-list/tag-list.component';
import { LicenseListComponent } from './license-list/license-list.component';
import { UserListComponent } from './user-list/user-list.component';

export const routes: Routes = [
  {
    component: AdminComponent,
    path: 'admin',
    children: [
      {
        path: '',
        component: MainComponent
      },
      {
        path: 'tag',
        component: TagListComponent
      },
      {
        path: 'license',
        component: LicenseListComponent
      },
      {
        path: 'user',
        component: UserListComponent
      }
    ]
  }
]

@NgModule({
  imports: [
    ComponentModule,
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [
    MainComponent, 
    AdminComponent, 
    TagListComponent,
    LicenseListComponent, 
    UserListComponent
  ]
})
export class AdminModule { }

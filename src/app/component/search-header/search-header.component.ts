import { Component, OnInit, Output, EventEmitter } from '@angular/core';
declare var require: any;

@Component({
  selector: 'app-search-header',
  templateUrl: './search-header.component.html',
  styleUrls: ['./search-header.component.css']
})
export class SearchHeaderComponent implements OnInit {

  logoImagePath = require('./assets/menu-4-20.png');
  searchIconPath = require('./assets/search.png');
  helpIconPath = require('./assets/help.png');
  notifyIconPath = require('./assets/notification.png');

  constructor() { }

  ngOnInit() {
  }

  @Output() onMenu = new EventEmitter<void>();
  @Output() onRightMenu = new EventEmitter<string>();
  showMenu() {
    this.onMenu.emit();
  }
  uploadShow() {
    this.onRightMenu.emit("upload");
  }
  advSrchShow() {
    this.onRightMenu.emit("advancedSearch");
  }
  notiShow() {
    this.onRightMenu.emit("notification");
  }
  settingsShow() {
    this.onRightMenu.emit("settings");
  }

}
